import { styled } from "@mui/material";

export const Gif = styled("div")`
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Div = styled("div")`
  height: 100vh;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  .header {
    width: 60%;
    max-width: 730px;
    display: flex;
    justify-content: space-around;
    align-items: center;
    @media (max-width: 480px) {
      justify-content: center;
    }
  }

  .content {
    height: 80%;
    display: grid;
    grid-template-columns: repeat(3, 1fr);

    @media (max-width: 620px) {
      grid-template-columns: repeat(2, 1fr);
    }

    @media (max-width: 480px) {
      grid-template-columns: 1fr;
    }
  }
`;
