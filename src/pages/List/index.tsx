import React, { useEffect, useState } from "react";
import { IList } from "../../@types/IList";
import { getList } from "../../api";
import Card from "../../components/Card";
import Logo from "../../components/Logo";
import OptionsBreeds from "../../components/OptionsBreeds";
import GetCookie from "../../hook/getCookie";
import { Div, Gif } from "./styled";
import logo from "../../assets/gifs/480.gif";
import { Button } from "@mui/material";
import RemoveCookie from "../../hook/removeCookie";
import { useNavigate } from "react-router-dom";

function List() {
  const initList = { breed: "padrao", list: [""] };
  const { token } = GetCookie("user");
  const [data, setData] = useState<IList>(initList);
  const [breed, setBreed] = useState("chihuahua");
  const navigate = useNavigate();
  let logado = data.list.length > 1 ? true : false;

  function logout() {
    RemoveCookie("user");
    navigate("/");
  }

  async function fetchList() {
    try {
      const response: IList | undefined = await getList(token, breed);

      response && setData(response);
    } catch (er) {
      console.log(er);
    }
  }

  useEffect(() => {
    fetchList();
  }, [breed]);

  if (!logado) {
    return (
      <Gif>
        <img src={logo} alt="" />
      </Gif>
    );
  }

  return (
    <Div>
      <section className="header">
        <Logo />
        <OptionsBreeds breed={breed} setBreed={setBreed} />
        <Button
          className="logout"
          variant="outlined"
          color="error"
          onClick={logout}
        >
          sair
        </Button>
      </section>

      <section className="content">
        {data.list.map((url, index) => {
          return <Card key={index} propUrl={url} />;
        })}
      </section>
    </Div>
  );
}

export default List;
