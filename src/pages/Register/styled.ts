import { styled } from "@mui/material";

export const Div = styled("div")`
  height: 100vh;
  width: 100%;
  section {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  section.logo {
    flex-direction: column;
    height: 20vh;
  }

  section.field-login {
    height: 80vh;
    flex-direction: flex;

    button {
      margin-left: 8px;
      padding: 12px;
    }
  }
`;
