import React, { useEffect, useState } from "react";
import { Button, TextField } from "@mui/material";
import { Div } from "./styled";
import Logo from "../../components/Logo";
import SetCookie from "../../hook/setCookie";
import GetCookie from "../../hook/getCookie";
import { useNavigate } from "react-router-dom";
import { postRegister } from "../../api/";

function Register() {
  const [email, setEmail] = useState("");
  let navigate = useNavigate();

  async function handleAuth() {
    const resText = await postRegister(email);

    if (!resText) return;

    SetCookie("user", resText);
    navigate("list");
  }
  useEffect(() => {
    const resCookie = GetCookie("user");
    resCookie ? navigate("list") : "";
  }, []);
  return (
    <Div>
      <section className="logo">
        <Logo />
      </section>
      <section className="field-login">
        <TextField
          id="email"
          aria-label="testEmail"
          label="digite seu email"
          variant="filled"
          onChange={(e) => setEmail(e.target.value)}
        />
        <Button onClick={handleAuth} variant="outlined">
          entrar
        </Button>
      </section>
    </Div>
  );
}

export default Register;
