import React from "react";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import List from "../pages/List";
import Register from "../pages/Register";

function Rotas() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Register />} />
        <Route path="register" element={<Register />} />
        <Route path="list" element={<List />} />
      </Routes>
    </BrowserRouter>
  );
}

export default Rotas;
