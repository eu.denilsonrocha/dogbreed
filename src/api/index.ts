import postRegister from "./postRegister";
import getList from "./getList";
const BASE_URL = "https://dogbreed-api.q9.com.br";

export { BASE_URL, getList, postRegister };
