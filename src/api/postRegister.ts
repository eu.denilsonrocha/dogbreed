import { BASE_URL } from ".";

async function postRegister(email: string) {
  try {
    const emailRegex = /\S+@\S+\.\S+/;
    if (!emailRegex.test(email)) return alert("e-mail inválido");
    const init = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ email }),
    };

    const res = await fetch(BASE_URL + "/register", init);
    const resText = await res.text();

    return resText;
  } catch (error) {
    console.log(error);
    return alert("nao deu certo");
  }
}

export default postRegister;
