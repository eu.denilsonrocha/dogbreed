import { BASE_URL } from ".";
import { IList } from "../@types/IList";

async function getList(
  token: string,
  breed: string = ""
): Promise<IList | undefined> {
  try {
    return await get();
  } catch (error) {
    console.log("Ops! algo deu errado na requisição " + error);
  }

  async function get() {
    const init = {
      method: "GET",
      headers: {
        Authorization: token,
        "Content-Type": "application/json",
      },
    };
    const res = await fetch(BASE_URL + "/list?breed=" + breed, init);
    const result = res && (await res.text()); // para ter certeza que veio string e poder fazer o parse
    const parserResult = JSON.parse(result);

    return parserResult.error ? backRegisterAlert : parserResult;
  }
}

function backRegisterAlert() {
  alert("Ops! algo deu errado.");

  window.location.href = "/";
}

export default getList;
