import Cookies from "js-cookie";

const SetCookie = (name: string, value: string) => {
  Cookies.set(name, value, {
    expires: 1,
  });
};

export default SetCookie;
