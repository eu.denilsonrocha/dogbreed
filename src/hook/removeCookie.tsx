import Cookies from "js-cookie";

const RemoveCookie = (name: string) => {
  Cookies.remove(name);
};

export default RemoveCookie;
