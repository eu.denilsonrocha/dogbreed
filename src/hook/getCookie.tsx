import Cookies from "js-cookie";
import { IResponseRegister } from "../@types/IUser";

const GetCookie = (name: string) => {
  const res = Cookies.get(name);
  const resParser: IResponseRegister = res ? JSON.parse(res) : "";

  if (!resParser) return resParser;

  const { user } = resParser;

  return user;
};

export default GetCookie;
