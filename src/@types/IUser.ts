export interface IUser {
  _id: string;
  email: string;
  token: string;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
}
export interface IResponseRegister {
  user: IUser;
}
