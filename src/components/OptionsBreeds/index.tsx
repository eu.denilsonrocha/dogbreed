import * as React from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { Div } from "./styled";

type PropOptionsBreed = {
  breed: string;
  setBreed: React.Dispatch<React.SetStateAction<string>>;
};
export default function OptionsBreeds({ breed, setBreed }: PropOptionsBreed) {
  const handleChange = (event: SelectChangeEvent) => {
    setBreed(event.target.value);
  };

  return (
    <Div>
      <FormControl variant="filled" sx={{ m: 1, minWidth: 120 }}>
        <InputLabel id="demo-simple-select-filled-label"> Raça</InputLabel>
        <Select
          labelId="demo-simple-select-filled-label"
          id="demo-simple-select-filled"
          value={breed}
          onChange={handleChange}
        >
          <MenuItem value="chihuahua">chihuahua</MenuItem>
          <MenuItem value="husky">husky</MenuItem>
          <MenuItem value="pug">pug</MenuItem>
          <MenuItem value="labrador">labrador</MenuItem>
        </Select>
      </FormControl>
    </Div>
  );
}
