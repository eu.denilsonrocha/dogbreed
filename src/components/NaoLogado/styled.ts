import { styled } from "@mui/system";

export const Div = styled("div")`
  width: inherit;
  height: inherit;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
