import Button from "@mui/material/Button";
import React from "react";
import { useNavigate } from "react-router-dom";
import { Div } from "./styled";

function NaoLogado() {
  const navigate = useNavigate();
  return (
    <Div>
      <p>Ops, você não está logado.</p>
      <Button onClick={() => navigate("/register")} variant="outlined">
        voltar
      </Button>
    </Div>
  );
}

export default NaoLogado;
