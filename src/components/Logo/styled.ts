import { styled } from "@mui/material";

export const Div = styled("div")`
  background: "white";
  color: ${({ theme }) => theme.palette.primary.main};
  display: flex;
  align-items: center;

  svg {
    color: black;
  }
`;

export const Dog = styled("h1")`
  color: #e8d5b5;
`;

export const Breed = styled("h1")`
  padding: 0 10px;
  color: #e19e20;
`;
