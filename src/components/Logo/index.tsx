import React from "react";
import { Breed, Div, Dog } from "./styled";
import PetsIcon from "@mui/icons-material/Pets";

const Logo = () => {
  return (
    <Div>
      <Dog>Dog</Dog>
      <Breed>Breed</Breed>
      <PetsIcon />
    </Div>
  );
};

export default Logo;
