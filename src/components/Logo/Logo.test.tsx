import React from "react";
import { render } from "@testing-library/react";
import Logo from "./index";

const { getByText } = render(<Logo />);
test("renders logo", () => {
  const dog = getByText(/dog/i);
  expect(dog).toBeInTheDocument();

  const breed = getByText(/breed/i);
  expect(breed).toBeInTheDocument();
});
