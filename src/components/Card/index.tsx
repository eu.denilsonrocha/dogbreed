import React, { useState } from "react";
import { Div } from "./styled";

type props = {
  propUrl: string;
};

function Card({ propUrl }: props) {
  return (
    <Div>
      <img src={propUrl} alt="cachorro" />
    </Div>
  );
}

export default Card;
