import { styled } from "@mui/material";

export const Div = styled("div")`
  width: 200px;
  height: 200px;
  margin: 4px;
  transition: all ease 0.52s;
  img {
    border-radius: 10px;
    width: inherit;
    height: inherit;
    object-fit: cover;
  }
  :hover {
    z-index: 1;
    transform: scale(1.2);
  }
`;
