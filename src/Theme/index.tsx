import { createTheme } from "@mui/material/styles";
import { red } from "@mui/material/colors";

// A custom theme for this app
const theme = createTheme({
  palette: {
    mode: "dark",
    primary: {
      main: "#FBF8FF",
      contrastText: "blue",
    },
    secondary: {
      main: "#19857b",
    },
    error: {
      main: red.A400,
    },
    background: {
      default: "#574F7D",
    },
    text: {
      primary: "#FBF8FF",
    },
  },
});

export default theme;
