# Desafio Dog Breed

Nesse diretório, você pode rodar os seguintes comandos:

### `npm start`

para rodar em ambiente de desenvolvimento.

### `npm test`

para rodar os testes.

### `npm run build`

para build do projeto.

### `npm run eject`

Se você não estiver satisfeito com a ferramenta de compilação e as opções de configuração, você pode `ejetar` a qualquer momento. Este comando removerá a dependência de compilação única do seu projeto.
